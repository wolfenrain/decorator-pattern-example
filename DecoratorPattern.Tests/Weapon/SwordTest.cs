using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DecoratorPattern.Tests
{
    [TestClass]
    public class SwordTest
    {
        private Sword sword;

        [TestInitialize]
        public void Initialize() {
            sword = new Sword("Sword");
        }

        [TestMethod]
        public void Damage()
        {
            Assert.AreEqual(sword.Damage(), 2);
        }
    }
}
