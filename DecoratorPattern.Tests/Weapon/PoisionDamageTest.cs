using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DecoratorPattern.Tests
{
    [TestClass]
    public class PoisonDamageTest
    {
        private PoisonDamage sword;

        [TestInitialize]
        public void Initialize() {
            sword = new PoisonDamage(new Sword("Sword"));
        }

        [TestMethod]
        public void Damage()
        {
            Assert.AreEqual(sword.Damage(), 4);
        }
    }
}

// deinstall, reboot en opnieuw installeren