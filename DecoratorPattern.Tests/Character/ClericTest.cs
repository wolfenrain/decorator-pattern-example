using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DecoratorPattern.Tests
{
    [TestClass]
    public class ClericTest
    {
        [TestMethod]
        public void TakeDamageHeals()
        {
            // initialize
            var human = new Cleric(new Human());

            // act
            human.TakeDamage(5);

            // assert
            Assert.IsTrue(human.HitPoints > 5);
        }

        [TestMethod]
        public void AttackHealsUnderTen()
        {
            // initialize
            var human = new Cleric(new Human());
            human.HitPoints = 5;

            // act
            human.Attack();

            // assert
            Assert.IsTrue(human.HitPoints > 5);
        }
    }
}
