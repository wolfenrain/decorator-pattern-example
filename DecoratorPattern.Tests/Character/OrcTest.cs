using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DecoratorPattern.Tests
{
    [TestClass]
    public class OrcTest
    {
        private Orc orc;

        [TestInitialize]
        public void Initialize()
        {
            orc = new Orc();
        }

        [TestMethod]
        public void IsProficientWithSwordTest()
        {
            // initialize
            var weapon = new Sword("Sword");

            // act
            var actual = orc.IsProficient(weapon);

            // assert
            var expected = false;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsProficientWithBowTest()
        {
            // initialize
            var weapon = new Bow("Bow");

            // act
            var actual = orc.IsProficient(weapon);

            // assert
            var expected = false;

            Assert.AreEqual(expected, actual);
        }
    }
}
