using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DecoratorPattern.Tests
{
    [TestClass]
    public class WarriorTest
    {
        private Warrior warrior;

        [TestInitialize]
        public void Initialize()
        {
            warrior = new Warrior(new Orc());
        }

        [TestMethod]
        public void IsProficientWithSwordTest()
        {
            // initialize
            var weapon = new Sword("Sword");

            // act
            var actual = warrior.IsProficient(weapon);

            // assert
            var expected = true;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsProficientWithBowTest()
        {
            // initialize
            var weapon = new Bow("Bow");

            // act
            var actual = warrior.IsProficient(weapon);

            // assert
            var expected = false;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AttackTest()
        {
            // initialize
            var weapon = new Bow("Bow");

            // act
            var actual = warrior.Attack();

            // assert
            Assert.IsTrue(warrior.Character.Attack() < actual);
        }
    }
}
