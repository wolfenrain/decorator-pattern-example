using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DecoratorPattern.Tests
{
    [TestClass]
    public class HumanTest
    {
        private Human human;

        [TestInitialize]
        public void Initialize()
        {
            human = new Human();
        }

        [TestMethod]
        public void IsProficientWithSwordTest()
        {
            // initialize
            var weapon = new Sword("Sword");

            // act
            var actual = human.IsProficient(weapon);

            // assert
            var expected = false;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsProficientWithBowTest()
        {
            // initialize
            var weapon = new Bow("Bow");

            // act
            var actual = human.IsProficient(weapon);

            // assert
            var expected = false;

            Assert.AreEqual(expected, actual);
        }
    }
}
