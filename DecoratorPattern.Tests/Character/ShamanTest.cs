using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DecoratorPattern.Tests
{
    [TestClass]
    public class ShamanTest
    {
        private Shaman shaman;

        [TestInitialize]
        public void Initialize()
        {
            shaman = new Shaman(new Orc());
        }

        [TestMethod]
        public void IsProficientWithSwordTest()
        {
            // initialize
            var weapon = new Sword("Sword");

            // act
            var actual = shaman.IsProficient(weapon);

            // assert
            var expected = true;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsProficientWithBowTest()
        {
            // initialize
            var weapon = new Bow("Bow");

            // act
            var actual = shaman.IsProficient(weapon);

            // assert
            var expected = true;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AttackTest()
        {
            // initialize
            var weapon = new Bow("Bow");

            // act
            var actual = shaman.Attack();

            // assert
            Assert.AreEqual(shaman.Character.Attack() + 5, actual);
        }
    }
}
