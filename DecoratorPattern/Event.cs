using System;

namespace DecoratorPattern
{
    enum EventType
    {
        Nothing,
        Battle,
        LevelUp,
    }
    class Event
    {   
        public EventType type;
        
        public Event(EventType type = EventType.Nothing) {
            this.type = type;
        }

        public static Event Random() {
            var val =  new Random().NextDouble();
            if (val < 0.5) {
                return new Event(EventType.Battle);
            }
            return new Event(EventType.Nothing);
        }

        static double BiasRandom(double bias) {
            var k = Math.Pow(1 - bias, 3);
            var rand = new Random();
            var x = rand.NextDouble();
            
            return (x * k) / (x * k - x + 1);
        }
    }
}