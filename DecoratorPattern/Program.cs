﻿using System;
using System.Collections.Generic;

namespace DecoratorPattern
{
    class Program
    {
        static List<string> firstPart = new List<string>{
            "Holy",
            "Eternal",
            "Evil",
            "Painful",
            "Cool",
            "Bloody"
        };

        static List<string> secondPart = new List<string>{
            "Doombringer",
            "Hellslayer",
            "Stick",
            "Weapon?",
            "Killing tool"
        };

        static Event currentEvent;

        static void Main(string[] args)
        {
            Character player = new Human();
            var firstName = firstPart[new Random().Next(firstPart.Count)];
            var lastName = secondPart[new Random().Next(secondPart.Count)];
            player.Weapon = new Bow($"{firstName} {lastName}");

            Console.WriteLine("If you want to exit the game just type 'Exit'\n\n");
            var response = Ask("Welcome adventurer! Are you ready to go on a decorative journey?", new List<string> { "Yes", "No" });
            if (response == 1)
            {
                // We loop indefinitely here because the Ask function is able to handle the exit.
                while (true)
                {
                    // Ensure we have a start event.
                    if (currentEvent == null)
                    {
                        currentEvent = Event.Random();
                        continue;
                    }

                    switch (currentEvent.type)
                    {
                        case EventType.Battle:
                            Character enemy;
                            if (new Random().NextDouble() < 0.5) { enemy = new Orc(); } else { enemy = new Human(); }

                            // Upgrade to Warrior.
                            if (new Random().NextDouble() < 0.5) { enemy = new Warrior(enemy); }

                            // Give it a weapon.
                            string wieldingWeapon = "";
                            if (new Random().NextDouble() < 0.5)
                            {
                                firstName = firstPart[new Random().Next(firstPart.Count)];
                                lastName = secondPart[new Random().Next(secondPart.Count)];
                                enemy.Weapon = new Sword($"{firstName} {lastName}");
                                wieldingWeapon = $" wielding {enemy.Weapon.Name}";
                            }

                            response = Ask($"You have entered a room with a {enemy}{wieldingWeapon}! What are you gonna do?", new List<string> { "Fight", "Flee" });

                            while (enemy.HitPoints >= 0)
                            {
                                if (response == 1)
                                {
                                    var damage = player.Attack();
                                    enemy.TakeDamage(damage);
                                    WriteLine($"You hit the {enemy} with your {player.Weapon.Name}! You deal {damage} damage");
                                    if (enemy.HitPoints > 0)
                                    {
                                        var weapon = enemy.Weapon != null ? enemy.Weapon.Name : "bare fists";
                                        damage = enemy.Attack();
                                        player.TakeDamage(damage);
                                        WriteLine($"The {enemy} attacks you with his {weapon}! He deals {damage} damage");
                                        if (player.HitPoints > 0)
                                        {
                                            response = Ask($"The {enemy} is still alive! What are you gonna do?", new List<string> { "Attack", "Flee" });
                                        }
                                        else
                                        {
                                            Exit("You died...");
                                        }
                                    }
                                }
                                else
                                {
                                    currentEvent = Event.Random();
                                    break;
                                }
                            }


                            if (enemy.HitPoints <= 0)
                            {
                                var experiencePoints = new Random().Next(3) + 1;
                                WriteLine($"The {enemy} is dead! You succeeded, you gained {experiencePoints} experience points!");
                                player.ExperiencePoints += experiencePoints;

                                if (new Random().NextDouble() < 0.3 && enemy.Weapon != null)
                                {
                                    var isProficient = player.IsProficient(enemy.Weapon) ? "you are proficient with this kind of weapon" : "you are not proficient with this kind of weapon";
                                    response = Ask($"The {enemy} weapon ({enemy.Weapon.Name}) is still here, {isProficient}.\nDo you want to equip it?", new List<string> { "Yes", "No" });
                                    if (response == 1)
                                    {
                                        player.Weapon = enemy.Weapon;
                                    }
                                }

                                if (player.ExperiencePoints >= 5)
                                {
                                    currentEvent = new Event(EventType.LevelUp);
                                    continue;
                                }
                            }
                            currentEvent = Event.Random();
                            continue;
                        case EventType.LevelUp:
                            response = Ask("You leveled up! You can upgrade your class, do you want to become a Warrior or a Cleric?", new List<string> { "Warrior", "Cleric" });
                            if (response == 1)
                            {
                                player = new Warrior(player);
                            }
                            else
                            {
                                player = new Cleric(player as Human);
                            }
                            player.ExperiencePoints = 0;
                            currentEvent = Event.Random();
                            continue;
                        case EventType.Nothing:
                        default:
                            // Chance to have a random weapon upgrade
                            if (new Random().NextDouble() < 0.3)
                            {
                                if (new Random().NextDouble() < 0.5)
                                {
                                    response = Ask($"You have entered a room with a vat of poison, what do you wanna do?", new List<string> { "Continue", "Rest", "Uh dip my weapon in the vat?" });

                                    if (response == 3)
                                    {
                                        player.Weapon = new PoisonDamage(player.Weapon);
                                        response = Ask($"Yes you may... You dip your weapon into the vat, it is now poisoned, what do you wanna do?", new List<string> { "Continue", "Rest" });
                                    }
                                }
                                else
                                {
                                    response = Ask($"You have entered a room with a fire in the middle, what do you wanna do?", new List<string> { "Continue", "Rest", "Can I try and put my weapon on fire?" });

                                    if (response == 3)
                                    {
                                        player.Weapon = new PoisonDamage(player.Weapon);
                                        response = Ask($"Sure why not... You put your weapon into the fire, well I'll be a monkey's uncle it is on fire, what do you wanna do?", new List<string> { "Continue", "Rest" });
                                    }
                                }
                            }
                            else
                            {
                                response = Ask($"You have entered an empty room, what do you wanna do?", new List<string> { "Continue", "Rest" });
                            }

                            while (response == 2)
                            {
                                if (player.HitPoints < 10)
                                {
                                    var healed = new Random().Next(10 - player.HitPoints) + 1;
                                    player.HitPoints += healed;
                                    var fullyHealed = player.HitPoints == 10 ? " You are fully healed." : "";
                                    response = Ask($"You took a rest, you healed {healed} points.{fullyHealed} Do you want to continue??", new List<string> { "Continue", "Rest" });
                                }
                                else
                                {
                                    response = Ask($"You took a rest. Do you want to continue??", new List<string> { "Continue", "Rest" });
                                }
                            }
                            WriteLine($"You continue going deeper into the dungeon");
                            currentEvent = Event.Random();
                            continue;
                    }
                }
            }
            else
            {
                Exit("Oh that is shame, byee");
            }
        }

        static void Exit(string exitMessage = "You coward")
        {
            Console.WriteLine(exitMessage);
            System.Environment.Exit(1);
        }

        static void WriteLine(string text)
        {
            Console.WriteLine($"  {text}");
        }

        static int Ask(string question, List<string> options)
        {
            string optionsStr = "";
            for (var i = 0; i < options.Count; i++)
            {
                optionsStr += $"({i + 1}) {options[i]}\n";
            }

            // Ask the question.
            Console.WriteLine(question);
            for (var i = 0; i < options.Count; i++)
            {
                Console.Write("\r" + new string(' ', Console.WindowWidth) + "\r");
            }
            Console.Write($"{optionsStr} > ");

            // Read the response.
            var response = Console.ReadLine();

            // Trim the input for validation.
            var val = response.Trim().ToLower();
            if (val == "exit")
            {
                Exit();
            }

            // Compare the input with the possible options, if none is found we ask the question again.
            int intValue;
            if (!int.TryParse(val, out intValue))
            {
                Console.WriteLine($"Sorry you can't use '{val}' here");
                return Ask(question, options);
            }

            if (options.Count >= intValue && intValue != -1)
            {
                // Clear the question line and replace it with the question and response.
                Console.SetCursorPosition(0, Console.CursorTop - (options.Count + 1));
                Console.Write("\r" + new string(' ', Console.WindowWidth) + "\r");
                Console.WriteLine($"({intValue}) {options[intValue - 1]}");
                return intValue;
            }

            Console.WriteLine($"Sorry you can't use '{val}' here");
            return Ask(question, options);
        }
    }
}
