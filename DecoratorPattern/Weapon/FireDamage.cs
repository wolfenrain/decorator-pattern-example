﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public class FireDamage : WeaponDecorator
    {
        public override string Name
        {
            get
            {
                if (Weapon.Name.Contains("<fire>"))
                {
                    return Weapon.Name;
                }
                return $"{Weapon.Name} <fire>";
            }
            set
            {
                Weapon.Name = value;
            }
        }

        public FireDamage(Weapon weapon) : base(weapon)
        {
            this.Weapon = weapon;
        }

        public override int Damage()
        {
            if (Weapon.Name.Contains("<fire>"))
            {
                return Weapon.Damage();
            }
            return Weapon.Damage() * 3;
        }
    }
}
