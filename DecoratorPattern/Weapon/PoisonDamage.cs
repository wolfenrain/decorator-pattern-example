﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public class PoisonDamage : WeaponDecorator
    {
        public override string Name
        {
            get
            {
                if (Weapon.Name.Contains("<poison>"))
                {
                    return Weapon.Name;
                }
                return $"{Weapon.Name} <poison>";
            }
            set
            {
                Weapon.Name = value;
            }
        }

        public PoisonDamage(Weapon weapon) : base(weapon)
        {
            this.Weapon = weapon;
        }

        public override int Damage()
        {
            if (Weapon.Name.Contains("<poison>"))
            {
                return Weapon.Damage();
            }
            return Weapon.Damage() * 2;
        }
    }
}
