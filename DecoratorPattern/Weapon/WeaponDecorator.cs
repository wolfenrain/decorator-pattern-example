﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public abstract class WeaponDecorator : Weapon
    {
        public Weapon Weapon { get; set; }
        
        public abstract string Name { get; set; }

        public WeaponDecorator(Weapon weapon)
        {
            this.Weapon = weapon;
        }

        public abstract int Damage();
    }
}
