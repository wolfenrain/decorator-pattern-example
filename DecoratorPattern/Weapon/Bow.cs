﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public class Bow : Weapon
    {
        public string Name { get; set; }

        public Bow(string Name)
        {
            this.Name = Name;
        }

        public int Damage()
        {
            return 3;
        }
    }
}
