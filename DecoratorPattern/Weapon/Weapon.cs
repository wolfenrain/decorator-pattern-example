﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public interface Weapon
    {
        string Name { get; set; }

        int Damage();
    }
}
