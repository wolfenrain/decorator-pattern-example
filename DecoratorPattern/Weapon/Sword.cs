﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public class Sword : Weapon
    {
        public string Name { get; set; }

        public Sword(string Name)
        {
            this.Name = Name;
        }

        public int Damage()
        {
            return 2;
        }
    }
}
