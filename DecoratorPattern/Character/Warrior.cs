﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public class Warrior : CharacterDecorator
    {
        public Warrior(Character character) : base(character)
        {
            this.Character = character;
        }

        public override bool IsProficient(Weapon weapon)
        {
            return weapon is Sword;
        }

        public override int Attack()
        {
            var randomValue = new Random().Next(2) + 1;
            // Bsae attack plus the weapon in combination with a random value because of warrior.
            return Character.Attack() + (Weapon != null ? Weapon.Damage() : 0) + randomValue;
        }
    }
}
