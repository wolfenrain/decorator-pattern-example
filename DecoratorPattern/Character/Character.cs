﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public abstract class Character
    {
        public int HitPoints = 10;

        public int ExperiencePoints = 0;

        public virtual Weapon Weapon { get; set; }

        public abstract bool IsProficient(Weapon weapon);

        public abstract int Attack();

        public void TakeDamage(int damage)
        {
            this.HitPoints -= damage;
        }

        public override string ToString() {
            return $"{GetType().Name}";
        }
    }
}
