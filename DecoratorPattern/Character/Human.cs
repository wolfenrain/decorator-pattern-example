﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public class Human : Character
    {
        public override int Attack()
        {
            return (Weapon != null ? Weapon.Damage() : 0) + 1; 
        }

        public override bool IsProficient(Weapon weapon)
        {
            return false;
        }
    }
}
