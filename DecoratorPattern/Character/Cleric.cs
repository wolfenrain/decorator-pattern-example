﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public class Cleric : CharacterDecorator
    {
        public Character Human { get; set; }

        public Cleric(Human human) : base(human)
        {
            this.Human = human;
        }

        public override bool IsProficient(Weapon weapon)
        {
            return Character.IsProficient(weapon);
        }

        public new void TakeDamage(int damage)
        {
            Character.TakeDamage(damage);

            // Randomly heal the charachter 2 points.
            HitPoints += new Random().Next(2) + 1;
        }

        public override int Attack()
        {
            // Secret healing ability. He be blessed yo!
            if (HitPoints < 10)
            {
                var healed = new Random().Next(10 - HitPoints) + 1;
                HitPoints += healed;
            }
            return Character.Attack() + (Weapon != null ? Weapon.Damage() : 0) + 1;
        }
    }
}
