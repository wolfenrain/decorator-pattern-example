﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public class Shaman : CharacterDecorator
    {
        public Character Orc { get; set; }

        public Shaman(Orc orc) : base(orc)
        {
            this.Orc = orc;
        }

        public override bool IsProficient(Weapon weapon)
        {
            return true; // Shamans are magical beings that somehow are really good with all wweapons.
        }

        public override int Attack()
        {
            return Character.Attack() + (Weapon != null ? Weapon.Damage() : 0) + 5;
        }
    }
}
