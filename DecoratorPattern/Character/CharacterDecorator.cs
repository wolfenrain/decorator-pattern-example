﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern
{
    public class CharacterDecorator : Character
    {
        public Character Character { get; set; }

        public new int HitPoints { get => Character.HitPoints; set => Character.HitPoints = value; }

        public new int ExperiencePoints { get => Character.ExperiencePoints; set => Character.ExperiencePoints = value; }

        public override Weapon Weapon { get => Character.Weapon; set => Character.Weapon = value; }

        public CharacterDecorator(Character character)
        {
            this.Character = character;
        }

        public override bool IsProficient(Weapon weapon) => Character.IsProficient(weapon);

        public override int Attack() => Character.Attack();

        public override string ToString() {
            return $"{Character.GetType().Name} <{GetType().Name}>";
        }
    }
}
