# decorator-pattern-example

A simple RPG game build using the decorator pattern.

Run `dotnet run --project DecoratorPattern` to execute.

Run `dotnet test` to run the tests.